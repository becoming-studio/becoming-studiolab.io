
var becomings = [
  "becoming.social",
  "becoming.internet",
  "becoming.change",
  "becoming.radical",
  "becoming.futures",
  "becoming.bots",
  "becoming.agency",
  "becoming.city",
  "becoming.critic",
  "becoming.action",
  "becoming.collective"
];

var fixed = false;

var font;

function preload() {
  font = loadFont('./assets/fonts/Archivo-SemiBold.ttf'); //din.ttf');
  //print("length" + abc.length);
}

function setup() {
  var c = createCanvas(600, 200);
  c.parent('logo');
  c.mouseMoved(us);
  c.mousePressed(us);

  textFont(font);
  textSize(60);

  frameRate(3);

}

function draw(){
  background(255);

  var i;
  if (fixed == true || mouseIsPressed)
    i = becomings.length-1;
  else
    i = floor(random(becomings.length-2));

  text(becomings[i], 0, 150);

  fixed = false;
}

function us(){
  fixed = true;
}
